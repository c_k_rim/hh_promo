/**
 * Интерфейс, описывающий конфигурацию для класса SimpleRequester
 * @interface
 */
export default interface SimpleRequesterConfigInterface {

    request: {
        url: string,
        params?: { [key: string]: string },
    }

    paths: {
        name: string,
        query: string,
        callback: (value: any) => string
    }[],
}