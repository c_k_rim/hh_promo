import SimpleRequesterConfigInterface from "./Interface/SimpleRequesterConfigInterface";

const fetch = require('node-fetch'); //Используем fetch
const jp = require('jsonpath'); //Используем jsonpath (https://github.com/dchester/jsonpath)

/**
 * Простой класс для упрощения работы с запросами к сторонним сервисам
 * а так же для упрощения парсинга json данных, возвращенного этими запросами
 * @class SimpleRequester
 * @export
 */
export default class SimpleRequester {

    /**
     * Метод, который собирает параметры ключ-значение в url-строку вида ?ключ=значение&...
     * @param params
     * @protected
     */
    protected buildParamString(params?: SimpleRequesterConfigInterface['request']['params']): string {

        if (typeof params === 'object' && Object.keys(params).length) {

            return '?' + Object.keys(params)
                .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(params[key]))
                .join('&');
        }

        return '';
    }

    /**
     * Асинхронный метод реализующий запрос к заданному url`у с параметрами (необязательно)
     * @param request
     * @async
     * @public
     */
    public async requestData(request: SimpleRequesterConfigInterface['request']): Promise<JSON> {


        const requestUrl = request.url + this.buildParamString(request.params),

            response = await fetch(requestUrl);

        return await response.json();
    }

    /**
     * Метод, позволяющий быстро выбрать и обработать данные из переданного json
     * @param json
     * @param paths
     * @public
     */
    public parseData(json: JSON, paths: SimpleRequesterConfigInterface['paths']): { [key: string]: string } {

        let result = {};

        paths.forEach(path => {
            const response = jp.apply(json, path.query, path.callback);

            result[path.name] = response[0].value;
        });

        return result;
    }

}