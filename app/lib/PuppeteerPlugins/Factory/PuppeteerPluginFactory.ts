import {PuppeteerPluginStore} from "../Store/PuppeteerPluginStore";

/**
 * Класс фабрика для создания плагинов Puppeteer
 * @export
 */
export class PuppeteerPluginFactory {

    /**
     * Статичный метод создания нового плагина
     * @param pluginName
     * @param config
     * @public
     * @static
     */
    public static createPlugin(pluginName: string, config) {
        if (PuppeteerPluginStore[pluginName] === undefined || PuppeteerPluginStore[pluginName] === null) {
            throw new Error(`Класс \'${pluginName}\' не найден в PuppeteerPluginStore`);
        }

        return new PuppeteerPluginStore[pluginName](config);
    }
}