import PuppeteerPluginsInterface from "./Interface/PuppeteerPluginsInterface";

import SimpleRequester from '../SimpleRequester/SimpleRequester';
import SimpleRequesterConfigInterface from "../SimpleRequester/Interface/SimpleRequesterConfigInterface";

import PuppeteerFacade from '../PuppeteerFacade';

/**
 * Класс - плагин для изменения зарплатного ожидания в резюме
 * в зависимости от текущего курса валют
 * @class CurrencyPuppeteerPlugin
 * @implements PuppeteerPluginsInterface
 * @export
 */
export default class CurrencyPuppeteerPlugin implements PuppeteerPluginsInterface {

    /**
     * Стандартное зарплатное ожидание
     * @protected
     */
    protected salary;

    /**
     * Конфигурация для requester`а
     * @protected
     */
    protected requesterConfig;

    /**
     * Конструктор класса
     * @param config
     * @public
     */
    public constructor(config: { requester: SimpleRequesterConfigInterface, plugin: { salary: number } }) {

        this.salary = config.plugin.salary;

        this.requesterConfig = config.requester;
    }

    /**
     * Сам процесс установки зарплатного ожидания в резюме
     * @param requester
     * @param pf
     * @async
     * @public
     */
    public async process(requester: SimpleRequester, pf: PuppeteerFacade): Promise<void> {

        const responseJson = await requester.requestData(this.requesterConfig.request);

        const exchangeRate = requester.parseData(responseJson, this.requesterConfig.paths).currency,

            salary = this.salary + parseFloat(exchangeRate) * 10;

        await pf.click('[data-qa="mainmenu_myResumes"]');

        await pf.click('[data-qa="resume-title-link"]');

        await pf.click('[data-qa="resume-block-position-edit"]');

        await pf.inputType('[data-qa="resume-salary-amount"]', salary);

        await pf.click('[data-qa="resume-submit"]');
    }
}