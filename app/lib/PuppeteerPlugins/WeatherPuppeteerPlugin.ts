import PuppeteerPluginsInterface from "./Interface/PuppeteerPluginsInterface";

import SimpleRequester from '../SimpleRequester/SimpleRequester';
import SimpleRequesterConfigInterface from "../SimpleRequester/Interface/SimpleRequesterConfigInterface";

import PuppeteerFacade from '../PuppeteerFacade';

/**
 * Класс - плагин, для изменения описания в резюме в зависимости от погоды
 * @class WeatherPuppeteerPlugin
 * @implements PuppeteerPluginsInterface
 * @export
 */
export default class WeatherPuppeteerPlugin implements PuppeteerPluginsInterface {

    /**
     * Стандартное описание
     * @protected
     */
    protected description;

    /**
     * Что необходимо указать в конце описания
     * @protected
     */
    protected after;

    /**
     * Конфигурация для requester`а
     * @protected
     */
    protected requesterConfig;


    /**
     * Конструктор класса
     * @param config
     * @public
     */
    public constructor(config: { requester: SimpleRequesterConfigInterface, plugin: { description: string, after: string } }) {

        this.description = config.plugin.description;
        this.after = config.plugin.after;

        this.requesterConfig = config.requester;
    }

    /**
     * Сам процесс изменения описания в резюме в резюме
     * @param requester
     * @param pf
     * @async
     * @public
     */
    public async process(requester: SimpleRequester, pf: PuppeteerFacade): Promise<void> {

        const responseJson = await requester.requestData(this.requesterConfig.request);

        const weather = requester.parseData(responseJson, this.requesterConfig.paths),

            newDescription = '\n'
                + this.description + Object.values(weather).join('\n')
                + '\n' + this.after;

        await pf.click('[data-qa="mainmenu_myResumes"]');

        await pf.click('[data-qa="resume-title-link"]');

        await pf.click('[data-qa="resume-block-skills-edit"]');

        await pf.inputType('[name="skills.string"]', newDescription);

        await pf.click('[data-qa="resume-submit"]');
    }
}