import CurrencyPuppeteerPlugin from "../CurrencyPuppeteerPlugin";
import WeatherPuppeteerPlugin from "../WeatherPuppeteerPlugin";

/**
 * Небольшой Store для хранения всех классов-плагинов
 */
export const PuppeteerPluginStore: any = {
    CurrencyPuppeteerPlugin,
    WeatherPuppeteerPlugin
};