import PuppeteerFacade from "../../PuppeteerFacade";
import SimpleRequester from "../../SimpleRequester/SimpleRequester";

/**
 * Интерфейс для классов-плагинов, использующих SimpleRequester и PuppeteerFacade
 * @interface
 */
export default interface PuppeteerPluginsInterface {

    process(requester: SimpleRequester, pf: PuppeteerFacade): Promise<void>;
}