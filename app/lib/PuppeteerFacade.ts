/**
 * Сама библиотека Puppeteer
 */
const Puppeteer = require('puppeteer');

/**
 * Простой фасад для упрощения работы с Puppeteer
 * @class PuppeteerFacade
 * @export
 */
export default class PuppeteerFacade {

    public browser;

    public page;

    /**
     * Асинхронный метод инициализации browser и page с переданными настройками
     * @param config
     * @async
     * @public
     */
    public async init(config: { args: string[] }): Promise<void> {

        this.browser = await Puppeteer.launch({
            args: config.args,
            headless: false
        });

        this.page = await this.browser.newPage();

        await this.page.emulateMedia('screen');

    }

    /**
     * Асинхронный метод "кликнуть" по переданному селектору
     * @param selector селектор elemet`а, по которому необходимо кликнуть
     * @async
     * @public
     */
    public async click(selector: string): Promise<void> {
        await this.page.waitForSelector(selector); //Ждем подгрузки элемента с переданным селектором
        await this.page.click(selector); //Кликаем по элементу
    }

    /**
     * Асинхронный метод "ввести текст в element по переданному селектору
     * @param selector селектрор element`а, в который необходимо ввести текст
     * @param value
     * @async
     * @public
     */
    public async inputType(selector: string, value): Promise<void> {

        await this.page.waitForSelector(selector);//Ждем подгрузки элемента с переданным селектором
        await this.page.focus(selector); //Уснатавливаем фокус на элемент

        /**
         * Очень похожим на человеческие действия, выделяем всё, в элементе, нажимаем
         * удалить. Этот процесс был сделан для более надёжного заполнения поля.
         * Поскольку существуют маски на элементах. Что бы с ними небыло
         * конфликтов - вводим посимвольно переданное сообщение.
         */
        await this.page.keyboard.down('Control');
        await this.page.keyboard.press('A');
        await this.page.keyboard.up('Control');
        await this.page.keyboard.press('Backspace');

        const chars = String(value).split('');

        for (let index = 0; index < chars.length; index++) {
            await this.page.keyboard.type(chars[index]);
        }
    }

    /**
     * Асинхронный метод "зактрыть всё"
     * Закрывает page и затем browser Puppeteer`а
     * @async
     * @public
     */
    public async closeAll(): Promise<void> {
        await this.page.close().catch(error => console.log(`Error closing page: ${error}.`));
        await this.browser.close();
    }
}