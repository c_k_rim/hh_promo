const Config = {

    puppeteer: {
        args: [
            '--disable-dev-shm-usage',
            '--unlimited-storage',
            '--full-memory-crash-report',
            '--no-sandbox',
            '--window-size=1920,1080'
        ]
    },

    hh: {
        entryUrl: 'https://sergievposad.hh.ru/login',

        login: 'gr.tsvetkov@gmail.com',
        password: 'xxx'
    },

    plugins: [
        {
            name: 'CurrencyPuppeteerPlugin',
            config: {
                requester: {
                    request: {
                        url: 'https://www.cbr-xml-daily.ru/daily_json.js',
                    },
                    paths: [
                        {
                            name: 'currency',
                            query: '$.Valute.USD.Value',
                            callback: (value): string => parseFloat(value).toFixed(1)
                        }
                    ]
                },

                plugin: {
                    salary: 120000
                }
            }
        },

        {
            name: 'WeatherPuppeteerPlugin',
            config: {
                requester: {
                    request: {
                        url: 'http://api.worldweatheronline.com/premium/v1/weather.ashx',
                        params: {
                            key: 'c82deefa50c04218a2f103845192406',
                            q: 'Moscow',
                            lang: 'ru',
                            format: 'json'
                        },
                    },

                    paths: [
                        {
                            name: 'outside',
                            query: '$.data.current_condition[0].lang_ru[0].value',
                            callback: (value): string => ' Cейчас у меня за окном ' + value.toLowerCase() + '.'
                        },
                        {
                            name: 'temp',
                            query: '$.data.current_condition[0].temp_C',
                            callback: (value): string => 'Температура примерно ' + value.toLowerCase() + '°.'
                        }
                    ],
                },

                plugin: {
                    description: 'Работа в ОС Linux. Установка\\настрока LAMP, LEMP, Sphinx и другое.\n' +
                        'Работа с PHP (PSR, SOLID), БД (MySQL, MongoDB), XSL,twig/другие шаблонизаторы,  JS (ES6, JQuery), Sphinx.\n' +
                        'Работа с фреймворком Meteor, Zend, Yii2, Laravel.\n' +
                        'CMS Joomla, Bitrix, Smarty, Netcat, Drupal, Wordpress.',

                    after: ' Скрипт, который меняет на странице моего резюме зарпалатные ожидания (в зависимости от курса доллара) и описание про погоду,' +
                        ' можно найти по адресу https://bitbucket.org/c_k_rim/hh_promo/ - это как пример моего кода. Часто просят git, что бы посмотреть на код,' +
                        ' вот я и написал на коленке за вечер.'
                }
            }
        },
    ]
};

export {Config};
