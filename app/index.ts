import {Config} from './config';

import PuppeteerFacade from './lib/PuppeteerFacade';
import SimpleRequester from './lib/SimpleRequester/SimpleRequester';

import CurrencyPuppeteerPlugin from './lib/PuppeteerPlugins/CurrencyPuppeteerPlugin';
import WeatherPuppeteerPlugin from './lib/PuppeteerPlugins/WeatherPuppeteerPlugin';
import {PuppeteerPluginFactory} from "./lib/PuppeteerPlugins/Factory/PuppeteerPluginFactory";

const pf = new PuppeteerFacade(),
    simpleRequester = new SimpleRequester();

let exitFlag = 0;

(async () => {

    await pf.init(Config.puppeteer); //Инициализируем puppeteer

    await pf.page.goto(Config.hh.entryUrl); //Заходим на страницу авторизации hh

    await pf.inputType('[name="username"]', Config.hh.login); //Вводим логин

    await pf.inputType('[name="password"]', Config.hh.password); //Вводим пароль

    await pf.page.click('[type="submit"]'); //Нажимаем "авторизоваться"


    for (const plugin of Config.plugins) {
        try {
            const pluginInstance = PuppeteerPluginFactory.createPlugin(plugin.name, plugin.config);

            await pluginInstance.process(simpleRequester, pf);

        } catch (e) {
            console.log(e.message);
            exitFlag = 1;
        }

    }

    await pf.closeAll();

    process.exit(exitFlag);
})();